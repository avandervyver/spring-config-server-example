package com.example.demo.spring.configclient.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RefreshScopeController {

	private ContextRefresher contextRefresher;

	@Autowired
	public RefreshScopeController(ContextRefresher contextRefresher) {
		this.contextRefresher = contextRefresher;
	}

	@GetMapping("/refresh")
	public String refreshScope() {
		contextRefresher.refresh();
		return "done";
	}
}
