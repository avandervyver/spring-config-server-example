package com.example.demo.spring.configclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class BeanOutputService {

	@Autowired
	private ExampleBean exampleBean;


	@Scheduled(fixedDelay = 3000L)
	public void writeItOut() {
		System.out.println(exampleBean.getValue());
	}

}
